(minetest.register_node "westfangar_ores:block_anathor"
                        {
                         "description" (.. "Anathor Block\n" "Can be placed")
                                       "paramtype2" "facedir"
                                       "place_param2" 0
                                       "tiles" ["westfangar_ores_block_anathor.png"]
                                       "is_ground_content" false
                                       "stack_max" 99
                                       "groups" {"cracky" 3
                                                          "crumbly" 1
                                                          "choppy" 1
                                                          "snappy" 1
                                                          }
                                       "sounds" (default.node_sound_stone_defaults)
                                       })

(minetest.register_craft {
                          "output" "westfangar_ores:block_anathor"
                                   "recipe" [["westfangar_ores:anathor_bar" "westfangar_ores:anathor_bar" "westfangar_ores:anathor_bar"]
                                             ["westfangar_ores:anathor_bar" "westfangar_ores:anathor_bar" "westfangar_ores:anathor_bar"]
                                             ["westfangar_ores:anathor_bar" "westfangar_ores:anathor_bar" "westfangar_ores:anathor_bar"]]
                                   })

(minetest.register_craft {
                          "output" "westfangar_ores:anathor_bar 9"
                                   "recipe" [["westfangar_ores:block_anathor"]]})
