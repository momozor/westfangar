(global westfangar_ores {})
(set westfangar_ores.path (minetest.get_modpath "westfangar_ores"))

(dofile (.. westfangar_ores.path "/ores.lua"))
(dofile (.. westfangar_ores.path "/bars.lua"))
(dofile (.. westfangar_ores.path "/oreblocks.lua"))
