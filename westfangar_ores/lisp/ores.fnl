(minetest.register_node "westfangar_ores:anathor_ore"
                        {
                        "description" (.. "Anathor Ore\n" "Can be placed")
                        "tiles" ["default_stone.png^westfangar_ores_anathor_ore.png"]
                        "inventory_image" "westfangar_ores_anathor_lump.png"
                        "stack_max" 99
                        "groups" {"cracky" 2}
                        "sounds" (default.node_sound_stone_defaults)
                        })

(minetest.register_ore {
                        "ore_type" "scatter"
                        "ore" "westfangar_ores:anathor_ore"
                        "wherein" "default:stone"
                        "clust_scarcity" (* 8 (* 8 8))
                        "clust_num_ores" 20
                        "clust_size" 4
                        "y_min" -31000
                        "y_max" 10
                        })

(minetest.register_craft {
                          "type" "cooking"
                          "cooktime" 15
                          "output" "westfangar_ores:anathor_bar"
                          "recipe" "westfangar_ores:anathor_ore"
                         })
