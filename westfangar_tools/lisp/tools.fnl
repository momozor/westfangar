;; Anathor

(minetest.register_tool "westfangar_tools:pickaxe_anathor"
                        {
                        "description" (.. "Anathor Pickaxe\n"
                                          "Melee Damage: 12\n"
                                          "Full Punch Interval: 0.82\n"
                                          "Range: 4.8")
                        "inventory_image" "westfangar_tools_pickaxe_anathor.png"
                        "range" 4.8
                        "tool_capabilities" {
                            "full_punch_interval" 0.82
                            "max_drop_level" 1
                            "groupcaps" {
                                "cracky" {
                                    "times" {
                                     '6 8
                                     '5 5.40
                                     '4 3.50
                                     '1 1.5
                                     '2 0.85
                                     '3 0.40
                                    }
                                    "uses" 90
                                    "maxlevel" 2
                                }
                                "crumbly" {
                                    "times" {
                                    '1 0.6
                                    '2 0.20
                                    '3 0.15
                                    }
                                    "uses" 90
                                    "maxlevel" 2
                                }
                                "choppy" {
                                    "times" {
                                    '6 20.0
                                    '5 14.40
                                    '4 10.50
                                    '1 8.0
                                    '2 5.50
                                    '3 2.85
                                    }
                                    "uses" 90
                                    "maxlevel" 2
                                }
                            }

                            "damage_groups" { "fleshy" 12 }
                        }

                        "sound" { "breaks" "default_tool_breaks" }
})


(minetest.register_craft {
                         "output" "westfangar_tools:pickaxe_anathor"
                         "recipe" [["wesfangar_ores:anathor_bar" "westfangar_ores:anathor_bar" "westfangar_ores:anathor_bar"]
                                   ["" "default:stick" ""]
                                   ["" "default:stick" ""]]
                         })

(minetest.register_tool "westfangar_tools:axe_anathor"
                        {

                        "description" (.. "Anathor Axe\n"
                                          "Melee Damage: 12\n"
                                          "Full Punch Interval: 0.82\n"
                                          "Range: 4.8")
                        "inventory_image" "westfangar_tools_axe_anathor.png"
                        "range" 4.8
                        "sound" { "breaks" "default_tool_breaks" }
                        "tool_capabilities" {
                            "full_punch_interval" 0.82
                            "max_drop_level" 1
                            "damage_groups" { "fleshy" 12 }
                            "groupcaps" {
                                "choppy" {
                                    "times" {
                                        '1 1.6
                                        '2 0.60
                                        '3 0.35
                                    }
                                    "uses" 90
                                    "maxlevel" 2
                                }
                            }
                        }


})

(minetest.register_craft {
                         "output" "westfangar_tools:axe_anathor"
                         "recipe" [["westfangar_ores:anathor_bar" "westfangar_ores:anathor_bar" ""]
                                   ["westfangar_ores:anathor_bar" "default:stick" ""]
                                   ["" "default:stick" ""]]
                         })

(minetest.register_tool "westfangar_tools:sword_anathor"
                       {
                       "description" (.. "Anathor Sword\n"
                                         "Melee Damage: 15\n"
                                         "Full Punch Interval: 0.62\n"
                                         "Range: 4.8")
                       "inventory_image" "westfangar_tools_sword_anathor.png"
                       "range" 4.8
                       "sound" { "breaks" "default_tool_breaks" }
                       "groupcaps" {
                            "snappy" {
                                "times" {
                                    '1 1.40
                                    '2 0.60
                                    '3 0.15
                                }
                                "uses" 90
                                "maxlevel" 2
                            }
                       }
})

(minetest.register_craft {
                         "output" "westfangar_tools:sword_anathor"
                         "recipe" [["" "westfangar_ores:anathor_bar" ""]
                                   ["" "westfangar_ores:anathor_bar" ""]
                                   ["" "default:stick" ""]]
})

