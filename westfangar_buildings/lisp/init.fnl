(global westfangar_buildings {})
(set westfangar_buildings.path (minetest.get_modpath "westfangar_buildings"))

(dofile (.. westfangar_buildings.path "/main.lua"))

(westfangar_buildings.build_all)
