westfangar_buildings.build_all = function()
  return westfangar_buildings.build_the_shire()
end
westfangar_buildings.build_the_shire = function()
  local filepath = (minetest.get_modpath("westfangar_buildings") .. "/media/the_shire_1.we")
  local file = io.open(filepath, "rb")
  local data_stream = file:read("*a")
  return file:close()
end
return westfangar_buildings.build_the_shire
